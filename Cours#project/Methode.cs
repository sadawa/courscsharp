﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cours_project
{
    internal class Methode
    {
        static void Main(string[] args)
        {
            SayHello("John");
            Console.WriteLine(Cube(5));
            Console.ReadLine();
        }
        static void SayHello(string name)
        {
            Console.WriteLine("Hello" + " " +name);
        }

        static int Cube(int num)
        {
            int result = num * num * num;
            return result;
        }

    }
}
