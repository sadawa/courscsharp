﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cours_project
{
    internal class ArrayCShatp
    {
        static void Main(string[] args)
        {
            int[] luckyNumbers = { 5, 10, 15, 20, 25, 30, 40, 50 };

            string[] friends = new string[3];
            friends[0] = "john";
            friends[1] = "Kim";
            friends[2] = "natalia";

            luckyNumbers[0] = 0;

            Console.WriteLine(luckyNumbers);
            Console.WriteLine(friends);

            Console.ReadLine();
        }
    }
}
