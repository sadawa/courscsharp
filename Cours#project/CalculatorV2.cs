﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cours_project
{
    internal class CalculatorV2
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entre a number :");
            double num1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter Operator :");
            string op = Console.ReadLine();

            Console.WriteLine("Entre another number :");
            double num2 = Convert.ToDouble(Console.ReadLine());

            if (op == "+") 
            { 
                Console.WriteLine(num1 + num2);
            } else if (op == "-") 
            {
            Console.WriteLine(num1 - num2);
            } else if (op == "*") 
            { 
                Console.WriteLine(num1 * num2);
            } else if (op == "/")
            {
                Console.WriteLine(num1 / num2);
            } else
            {
                Console.WriteLine("You need a operator + - * or / for see a result");
            }
            
            Console.ReadLine();
        }
    }
}
