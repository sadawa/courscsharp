﻿En C#, pour créer une fonction, tu dois définir ce qu'on appelle une méthode au sein d'une classe ou d'un struct. Voici les étapes de base pour écrire une fonction :

Définir la visibilité : public, private, protected, ou internal. public signifie que la méthode peut être appelée de n'importe où, private qu'elle ne peut être appelée que depuis la classe ou le struct où elle est définie.
Indiquer le type de retour : Le type de la valeur que la méthode va retourner. Si la méthode ne retourne rien, tu utilises le mot-clé void.
Nommer la méthode : Utilise un nom descriptif pour ta méthode.
Paramètres : Entre parenthèses, tu peux définir zéro ou plusieurs paramètres que la méthode peut prendre, séparés par des virgules.
Corps de la méthode : Entre accolades, tu écris le code qui définit ce que fait la méthode.
Voici un exemple simple :

csharp

public class Operations
{
    // Méthode qui calcule la somme de deux nombres
    public int Add(int a, int b)
    {
        return a + b; // retourne la somme de a et b
    }

    // Méthode sans retour (void) qui affiche un message
    public void DisplayMessage(string message)
    {
        Console.WriteLine(message);
    }
}
Dans cet exemple, nous avons une classe Operations qui contient deux méthodes :

Add qui prend deux entiers en paramètres et retourne leur somme.
DisplayMessage qui prend une chaîne de caractères en paramètre et affiche ce message à l'écran.
Tu peux ensuite utiliser ces méthodes de la manière suivante :

csharp

class Program
{
    static void Main()
    {
        Operations ops = new Operations();
        int result = ops.Add(5, 3);
        Console.WriteLine("Le résultat est : " + result);

        ops.DisplayMessage("Bonjour tout le monde!");
    }
}
Cet exemple montre comment instancier la classe Operations, appeler ses méthodes et travailler avec les résultats.


----


En C#, une méthode ou fonction est un bloc de code qui effectue une opération spécifique. Elle peut prendre des paramètres, exécuter des instructions, et retourner une valeur. Les méthodes sont définies à l'intérieur de classes ou de structs, et elles sont essentielles pour organiser et réutiliser le code. Voici comment tu peux définir et utiliser des méthodes en C#.

1. Définition d'une Méthode
Pour définir une méthode, tu as besoin de spécifier plusieurs éléments :

Visibilité (Access Modifier) : Détermine où la méthode peut être appelée. Les plus courants sont public, private, protected, et internal.
Type de Retour : Le type de donnée que la méthode renvoie. Si la méthode ne retourne rien, utilise void.
Nom de la Méthode : Un identifiant unique pour la méthode.
Paramètres : Les entrées nécessaires pour que la méthode effectue son travail, spécifiées entre parenthèses après le nom de la méthode.
Exemple de Définition de Méthode
Voici un exemple de classe avec deux méthodes :

csharp

public class Calculator
{
    // Méthode qui calcule la somme de deux entiers
    public int Add(int a, int b)
    {
        return a + b;
    }

    // Méthode qui affiche le résultat d'une addition
    public void DisplaySum(int a, int b)
    {
        int result = Add(a, b);
        Console.WriteLine("La somme de {0} et {1} est : {2}", a, b, result);
    }
}
2. Utilisation d'une Méthode
Après avoir défini une méthode, tu peux l'appeler à partir d'autres parties de ton code. Pour appeler une méthode, tu utilises le nom de la méthode suivi des paramètres requis entre parenthèses.

Exemple 1 : Appeler une Méthode qui Retourne une Valeur
csharp

Calculator myCalculator = new Calculator();
int sum = myCalculator.Add(5, 7);
Console.WriteLine("Le résultat est : " + sum);
Dans cet exemple, Add est appelée avec deux arguments, 5 et 7, et le résultat est stocké dans la variable sum.

Exemple 2 : Appeler une Méthode qui N’a pas de Retour
csharp

Calculator myCalculator = new Calculator();
myCalculator.DisplaySum(5, 7); // Affiche directement le résultat
Dans cet exemple, DisplaySum est appelée, qui à son tour appelle Add pour obtenir le résultat, puis affiche ce résultat.

Points Clés
Les méthodes en C# permettent de regrouper des instructions qui exécutent une tâche spécifique.
La déclaration de méthode inclut la visibilité, le type de retour, le nom, et les paramètres.
Les méthodes peuvent retourner des valeurs ou être de type void pour ne rien retourner.
Appeler une méthode se fait en utilisant son nom et en passant les arguments nécessaires.
Ces principes te permettent de structurer efficacement ton code en C#, facilitant ainsi sa maintenance et sa compréhension.


